#!/bin/sh
while ! nc -z tss0 8080; do
  echo sleeping
  sleep 1
done
BOOTSTRAP_HOST=$(nslookup tss0 | awk '/^Address: / {print $2}')
echo $PRIVKEY | /go/bin/tss -tss-port :8080 -peer /ip4/$BOOTSTRAP_HOST/tcp/6668/p2p/$(curl http://tss0:8080/p2pid) -p2p-port 6668 -loglevel debug -walletrpcA $WALLET_RPC_A -walletrpcB $WALLET_RPC_B
