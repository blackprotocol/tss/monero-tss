## These are the keys used for test
```bash
KEYS=('blkpub1addwnpepq0gaaux7tehfezwqp3nm3y44gpry4wqwq53wwvxgnrw7d9wsvthqcn03nyg',
'blkpub1addwnpepqw2l09wfm3wwnepq9ss8q7hds3dvxzr6x4n3tfm0lvgced2qkjtzydh8csw',
'blkpub1addwnpepqggv3elhvaqh47rvlcv2dhvq8r4e8ge48we3f65e8x40v880dfqyxzpugew',
'blkpub1addwnpepqwklrrzqv4k5r7ld29pnu0xkm7pefam3hr9tnksqnd9l8r6cmhvcvjgk5p0',
'blkpub1addwnpepq2k03qukuxzrl5rnnnd32rccx3f33jhfe40ulg2h3mgsazf62kk9x8cy9u8',
'blkpub1addwnpepqgjgdd6rfcd0m569tah3ldt2qy8agrhpr8lmgdycpvhraf6ht58jq2cq0hg',
'blkpub1addwnpepqwjwdmcuxsyfk08d5m7m4xq5q02rht0dsfd5k7c25nedcx82ly8gvscugqc',
'blkpub1addwnpepqw2q4jp576ypcf6ttv9cjp50cxz0y8dmde7azfw5tvtats7dhyf7jtawakd',
'blkpub1addwnpepqgmt7zs6cwjz4rzww6q7ds3teyq3wff0rhueuuf4xndvlk4mxp4j7med3e9',
'blkpub1addwnpepq0s94kq4sp6yrtczu63palyy9a0eklw5mnmddlcld8hlk4s02uv52gdjtfl',
'blkpub1addwnpepq0wf4nenvft6rjl9726cnqa32jd3xfdsg435963esm0322mkq60hxwwcj0s',
'blkpub1addwnpepqfctegrzlj0kkxgywu6dryhlpqrqut0ksed79tzkakcf02x767qgs7hant8',
'blkpub1addwnpepqwg3ff5ht4ja4aypndv2pc5m3qhu52t4a66uuvmxv7fx5pwnljshq5lnhh2',
'blkpub1addwnpepq076y8xzg7gpr3tzhrqx9y2a597cgnumgazywdmwt28h4g8999tcwgn7z2q',
'blkpub1addwnpepqfs00dvnn9k8cd7ge8nhr96v62mjpz6j6fnw25pe7ezgc3eg6jqe7tlx84s',
'blkpub1addwnpepqvdvwkz5l9892rkd6ajdcqeaq5ffz2vtk07kt2hlahccm5vyj7ne7vj5yc3',
'blkpub1addwnpepqt0hkvgarg9xxre7qax0ah6ftfpgrywrn0hkhgu2hycxsmsfx7xwxpmnngt',
'blkpub1addwnpepqw9fvrzcvm7xf2464t97ht370axfhd23fkm7vy20vc56glqq902pxnsfpq4',
'blkpub1addwnpepqflhdv3qemh6zpux9mnjmd7zwhuapsmgg440t5ah8d8u6k652yaw72a40yl',
'blkpub1addwnpepqt05w0qnlp2vd3lhztn2pxu5554sk62wg4rreqayqgeetmz8fr43gcg4r87')
export TSS_0='YjBmYzFkNjQ3Njg1ZThiNmEzZDgzY2UxNGIzYjM1Mjk3ZDE4ZWMxYmE4ZDQ1OTY0MGE4ZmM5OWM3OWJiMjc3MA=='
export TSS_1='MTViNTAzMTU4MWU1OGExNWFmY2IzNjJhYzU4ZmJhMmNjOWVlMTVmMGRhMjFiYWJhMWQ0ZDQ3OGE4ZDVhZTkyNg=='
export TSS_2='MDdmNjU5NTFkOGE0NjRmZGU4ZjI5OTViZGRhMTNlZjM1NDAxZTNjYWYyMzM1YzdkZTRjOWI2ZjgxZGUyY2E2OA=='
export TSS_3='OTAzODk0NTcxMWNlYjg1MDZiOTdjNGIxMzRjZjk0M2Y3ZTY3OTExZWM0NWVhM2Q3MWI4MzdiMWUxNDg4MmE1Zg=='
export TSS_4='ZTE4NTNiYzc1ZmUxMjgzMjAyMmQyY2ViMjgyODgwODI4MTgzMWY4ZDQwNTcxMjJmNDIyYzY1MTBmZDJjMmUxNQ=='
export TSS_5='ZDYzYjYxZGQxMDNjNjM3MDE0ZGRhZTVjNDJkMDMyMjYwMjJjOTk5YjI2MGUwYTAyM2ZjMzc1MjBlODRhM2E0OQ=='
export TSS_6='ZTE4N2Q4ZjljN2Y3ODc3NzAzZjRjNWY2ZDkzMmJiOTA2YTU5NjhmOGE4ZmE0NTM4Y2YwZDQ4MjM1ZmNlMDFmNA=='
export TSS_7='YWI4YWQxY2Y3YjJiZGQ0YTFkMDEwZWFlZDQzNzVhZTcwM2FkM2VmMGY5NjZjZTYxZDY0OGU2NmVhNWNhN2EzOQ=='
export TSS_8='NWQxNmMyYmJhMDgyOTA1MzEyNTgwYWFmNDY5ZmIwYTRhMmY4ZDhlMWNhOGYwOTJkNTE2ZTc3OTNjNjczNjk2NQ=='
export TSS_9='ZTE5OGQwNjEzYmQ4MTk1ZDBiM2RiYzRhMzA3ZTMwMmZlYmU2Yjc3Y2EyNWVlY2Y4MWVmZGJjMTI5MDA1OTEyMA=='
export TSS_10='NjJlOTJlNzA1NDYxYWMzYWNhNDk2NDZjZDAyMTIwYjAyODA5MzYxNWE2MzNjMmJiOTNlYzg0MmQ2ZDNjMDFlNw=='
export TSS_11='NDJkZGU5MTk3ZDUwY2ZmZDdmZDI1YTVmZmMyNDViMDE5NTE4ZWJlMzk1OWJkMjQ1NTUzZDBlOGNjMTgxMmE2NQ=='
export TSS_12='NjhjMjhlOTcxZWZmNDI4MjVlOTc5YTliOGJkMDlmZjM2OGE2YWJiMmY4NTBiODQ4ZDNmOTMzMGNjZGZhMDRkMQ=='
export TSS_13='ZGJlN2I3MDFjZjk4YmEyZTc2YzYzMWI0MzE2NWU3OGI2Y2I0YWE3NDhkN2Q0YzIwMmZiNzg5YjVhYzVkMmM3Yw=='
export TSS_14='MzgyODI3MTk2YzI1MjE5NzU4MjE4MzJhOWI4ZWM3NGRlZjQyZDMxYTQyOWU1Yzc1YWYzYzUxMDk0NjMzYjMwMA=='
export TSS_15='MjliNDZhZTg2NTY0MjcxYzE1YzIwZDY1M2NkYTZhZGYwNjA3NTRlZWU1ZjY4ZmUyNjUyYzQyMmQzYzQ5NzUyNQ=='
```
