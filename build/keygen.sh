#!/bin/sh
set -ex
KEYS=('blkpub1addwnpepq0gaaux7tehfezwqp3nm3y44gpry4wqwq53wwvxgnrw7d9wsvthqcn03nyg','blkpub1addwnpepqw2l09wfm3wwnepq9ss8q7hds3dvxzr6x4n3tfm0lvgced2qkjtzydh8csw','blkpub1addwnpepqggv3elhvaqh47rvlcv2dhvq8r4e8ge48we3f65e8x40v880dfqyxzpugew','blkpub1addwnpepqwklrrzqv4k5r7ld29pnu0xkm7pefam3hr9tnksqnd9l8r6cmhvcvjgk5p0')
#'blkpub1addwnpepq2k03qukuxzrl5rnnnd32rccx3f33jhfe40ulg2h3mgsazf62kk9x8cy9u8',
#'blkpub1addwnpepqgjgdd6rfcd0m569tah3ldt2qy8agrhpr8lmgdycpvhraf6ht58jq2cq0hg',
#'blkpub1addwnpepqwjwdmcuxsyfk08d5m7m4xq5q02rht0dsfd5k7c25nedcx82ly8gvscugqc',
#'blkpub1addwnpepqw2q4jp576ypcf6ttv9cjp50cxz0y8dmde7azfw5tvtats7dhyf7jtawakd',
#'blkpub1addwnpepqgmt7zs6cwjz4rzww6q7ds3teyq3wff0rhueuuf4xndvlk4mxp4j7med3e9',
#'blkpub1addwnpepq0s94kq4sp6yrtczu63palyy9a0eklw5mnmddlcld8hlk4s02uv52gdjtfl',
#'blkpub1addwnpepq0wf4nenvft6rjl9726cnqa32jd3xfdsg435963esm0322mkq60hxwwcj0s',
#'blkpub1addwnpepqfctegrzlj0kkxgywu6dryhlpqrqut0ksed79tzkakcf02x767qgs7hant8',
#'blkpub1addwnpepqwg3ff5ht4ja4aypndv2pc5m3qhu52t4a66uuvmxv7fx5pwnljshq5lnhh2',
#'blkpub1addwnpepq076y8xzg7gpr3tzhrqx9y2a597cgnumgazywdmwt28h4g8999tcwgn7z2q',
#'blkpub1addwnpepqfs00dvnn9k8cd7ge8nhr96v62mjpz6j6fnw25pe7ezgc3eg6jqe7tlx84s',
#'blkpub1addwnpepqvdvwkz5l9892rkd6ajdcqeaq5ffz2vtk07kt2hlahccm5vyj7ne7vj5yc3')
HEIGHT=$(date +%s)
PAYLOAD=$(jq --null-input \
--arg keys "$KEYS" \
--arg height "$HEIGHT" \
'{"keys": $keys | split(","),   "block_height": $height | fromjson, "version": "v1.0.0" }')
PORT=8080

for i in {0..3}
do
  curl --data "$PAYLOAD" http://localhost:$((PORT+i))/keygen &
done