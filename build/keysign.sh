#!/bin/sh
set -ex
# make sure currently opened wallet is the multisig wallet
# get the address of currently opened wallet
#ADDRESS=$(supertools monero get_address --account_index 0 --address_index 0 --short)
HEIGHT=$(date +%s)
# shellcheck disable=SC2037
AMOUNT=$(( HEIGHT * 100 ))
TX=$(supertools monero request_transfer --recipient "48AEAM52aQKA5MX4Mtpu97VwQPDH4oZjMMgHA6rXAADbZDzTY7ceCMm7QJzLonWZCqStpFNDzCqx29qW87fquLXj6f19xX4" --amount $AMOUNT)
PAYLOAD=$(jq --null-input \
--arg height "$HEIGHT" \
--arg address "$ADDRESS" \
--arg tx "$TX" \
'{"wallet_address": $address, "encoded_transaction": $tx, "block_height": $height | fromjson, "version": "v1.0.0" }')
PORT=8080

for i in {0..3}
do
  curl --data "$PAYLOAD" http://localhost:$((PORT+i))/keysign &
done