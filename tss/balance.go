package tss

import (
	"fmt"

	"gitlab.com/blackprotocol/monero-rpc/wallet"

	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig"
)

// GetBalance will used the given wallet address to figure out which wallet rpc instance it need to send the request to
// and then proxy the request to the wallet instance , return the result.
func (t *ServerImp) GetBalance(walletAddress string) (*wallet.ResponseGetBalance, error) {
	localStateItem, err := t.stateManager.GetLocalWalletState(walletAddress)
	if err != nil {
		return nil, fmt.Errorf("fail to get local keygen state: %w", err)
	}
	var walletAccessor monero_multi_sig.WalletAccessor
	if t.walletAccessorCreator == nil {
		walletAccessor = monero_multi_sig.DefaultWalletAccessorCreator(localStateItem.WalletRPC)
	} else {
		walletAccessor = t.walletAccessorCreator(localStateItem.WalletRPC)
	}
	return walletAccessor.GetBalance(&wallet.RequestGetBalance{
		AccountIndex:   0,
		AddressIndices: nil,
	})
}
