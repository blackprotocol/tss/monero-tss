package tss

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/libp2p/go-libp2p/core/peer"
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"

	"gitlab.com/blackprotocol/tss/go-tss/common"
	"gitlab.com/blackprotocol/tss/go-tss/conversion"
	"gitlab.com/blackprotocol/tss/go-tss/messages"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"

	"gitlab.com/blackprotocol/tss/monero-tss/blame"
	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig"
	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig/keysign"
)

func (t *ServerImp) waitForSignatures(msgID, encodedTx, memo string, walletAccessor monero_multi_sig.WalletAccessor, sigChan chan string, threshold int) (keysign.Response, error) {
	data, err := t.signatureNotifier.WaitForSignature(msgID, encodedTx, memo, walletAccessor, t.conf.KeySignTimeout, sigChan, threshold)
	if err != nil {
		return keysign.Response{}, err
	}
	if data == nil {
		return keysign.Response{}, errors.New("keysign failed with nil signature")
	}
	return keysign.NewResponse(
		data.TransactionID,
		data.Signature,
		common.Success,
		tb.Blame{},
	), nil
}

func (t *ServerImp) generateSignature(msgID string,
	req keysign.Request,
	threshold int,
	allParticipants []string,
	blameMgr *blame.Manager,
	keysignInstance *keysign.MoneroKeySign,
	sigChan chan string) (keysign.Response, error) {
	allPeersID, err := conversion.GetPeerIDsFromPubKeys(allParticipants)
	if err != nil {
		t.logger.Err(err).Msg("invalid request")
		return keysign.Response{
			Status: common.Fail,
			Blame:  tb.NewBlame(tb.InternalError, []tb.Node{}),
		}, nil
	}

	joinPartyStartTime := time.Now()
	onlinePeers, leader, errJoinParty := t.joinParty(msgID, req.Version, req.BlockHeight, allParticipants, threshold, sigChan)
	joinPartyTime := time.Since(joinPartyStartTime)
	if errJoinParty != nil {
		// we received the signature from waiting for signature
		if errors.Is(errJoinParty, p2p.ErrSignReceived) {
			return keysign.Response{}, errJoinParty
		}
		t.tssMetrics.KeysignJoinParty(joinPartyTime, false)

		var blameLeader tb.Blame
		leaderPubKey, err := conversion.GetPubKeyFromPeerID(leader)
		if err != nil {
			t.logger.Err(errJoinParty).Msgf("fail to convert the peerID to public key %s", leader)
			blameLeader = tb.NewBlame(tb.TssSyncFail, []tb.Node{})
		} else {
			blameLeader = tb.NewBlame(tb.TssSyncFail, []tb.Node{{leaderPubKey, nil, nil}})
		}

		t.broadcastKeysignFailure(msgID, allPeersID)
		// make sure we blame the leader as well
		t.logger.Err(errJoinParty).
			Str("message_id", msgID).Interface("online_peers", onlinePeers).
			Msg("fail to form keysign party with online peers")
		return keysign.Response{
			Status: common.Fail,
			Blame:  blameLeader,
		}, nil
	}
	t.tssMetrics.KeysignJoinParty(joinPartyTime, true)
	isKeySignMember := false
	for _, el := range onlinePeers {
		if el == t.p2pCommunication.GetHost().ID() {
			isKeySignMember = true
		}
	}
	if !isKeySignMember {
		t.logger.Info().Msgf("%s is not the active signer", t.p2pCommunication.GetHost().ID().String())
		return keysign.Response{}, p2p.ErrNotActiveSigner
	}
	t.logger.Info().Msgf("party formed , online peers: %+v", onlinePeers)
	parsedPeers := make([]string, len(onlinePeers))
	for i, el := range onlinePeers {
		parsedPeers[i] = el.String()
	}

	signers, err := conversion.GetPubKeysFromPeerIDs(parsedPeers)
	if err != nil {
		t.logger.Err(err).Msg("fail to get public keys")
		sigChan <- "signature generated"
		return keysign.Response{
			Status: common.Fail,
			Blame:  tb.Blame{},
		}, nil
	}

	signedTx, err := keysignInstance.SignMessage(req.EncodedTx, signers, req.Memo)

	// the statistic of keygen only care about Tss itself, even if the following http response aborts,
	// it still counted as a successful keygen as the Tss model runs successfully.
	// as only the last node submit the signature, others will return nil of the signedTx
	if err != nil {
		t.logger.Error().Err(err).Msg("err in keysign")
		t.broadcastKeysignFailure(msgID, allPeersID)
		sigChan <- "signature generated"
		blameNodes := *blameMgr.GetBlame()
		return keysign.Response{
			Status: common.Fail,
			Blame:  blameNodes,
		}, tb.ErrTssTimeOut
	}

	// this indicates we are not the last node who submit the transaction
	if signedTx == nil {
		t.logger.Info().Msgf("Sign Message Returned empty signedTx")
		return keysign.NewResponse(
			"",
			"",
			common.Fail,
			tb.Blame{},
		), errors.New("not the final signer")
	}

	// When signedTx is empty , it knows that the keysign is successful , however it failed to broadcast the tx to chain
	// reason been someone else in the same signing party did it successfully. Assume they will broadcast the signature
	if !signedTx.IsEmpty() {
		// update signature notification
		if err := t.signatureNotifier.BroadcastSignature(msgID, signedTx, allPeersID); err != nil {
			return keysign.Response{}, fmt.Errorf("fail to broadcast signature:%w", err)
		}
		// tell signature notifier to stop waiting for signatures
		sigChan <- "signature generated"
	}

	return keysign.NewResponse(
		signedTx.TransactionID,
		signedTx.Signature,
		common.Success,
		tb.Blame{},
	), nil
}

func (t *ServerImp) updateKeySignResult(result keysign.Response, timeSpent time.Duration) {
	if result.Status == common.Success {
		t.tssMetrics.UpdateKeySign(timeSpent, true)
		return
	}
	t.tssMetrics.UpdateKeySign(timeSpent, false)
}

func (t *ServerImp) KeySign(req keysign.Request) (keysign.Response, error) {
	t.logger.Info().
		Int64("block_height", req.BlockHeight).
		Str("wallet_address", req.WalletAddress).
		Str("tx", req.EncodedTx).
		Msg("keysign request")
	emptyResp := keysign.Response{}
	msgID, err := t.requestToMsgID(req)
	if err != nil {
		return emptyResp, err
	}
	localStateItem, err := t.stateManager.GetLocalWalletState(req.WalletAddress)
	if err != nil {
		return emptyResp, fmt.Errorf("fail to get local keygen state: %w", err)
	}
	var walletAccessor monero_multi_sig.WalletAccessor
	if t.walletAccessorCreator == nil {
		walletAccessor = monero_multi_sig.DefaultWalletAccessorCreator(localStateItem.WalletRPC)
	} else {
		walletAccessor = t.walletAccessorCreator(localStateItem.WalletRPC)
	}
	keysignInstance, err := keysign.NewMoneroKeySign(t.p2pCommunication.GetLocalPeerID(),
		t.conf,
		t.p2pCommunication.BroadcastMsgChan,
		t.stopChan, msgID,
		t.privateKey,
		t.p2pCommunication,
		walletAccessor,
		localStateItem.WalletName)
	if err != nil {
		return keysign.Response{}, fmt.Errorf("fail to create monero keysign instance, err: %w", err)
	}

	keySignChannels := keysignInstance.GetTssKeySignChannels()
	t.p2pCommunication.SetSubscribe(messages.TSSKeySignMsg, msgID, keySignChannels)
	t.p2pCommunication.SetSubscribe(messages.TSSKeySignVerMsg, msgID, keySignChannels)
	t.p2pCommunication.SetSubscribe(messages.TSSControlMsg, msgID, keySignChannels)
	t.p2pCommunication.SetSubscribe(messages.TSSTaskDone, msgID, keySignChannels)

	defer func() {
		t.p2pCommunication.CancelSubscribe(messages.TSSKeySignMsg, msgID)
		t.p2pCommunication.CancelSubscribe(messages.TSSKeySignVerMsg, msgID)
		t.p2pCommunication.CancelSubscribe(messages.TSSControlMsg, msgID)
		t.p2pCommunication.CancelSubscribe(messages.TSSTaskDone, msgID)

		t.p2pCommunication.ReleaseStream(msgID)
		t.signatureNotifier.ReleaseStream(msgID)
		t.partyCoordinator.ReleaseStream(msgID)
	}()

	walletInfo, err := walletAccessor.IsMultisig()
	if err != nil {
		t.logger.Error().Err(err).Msgf("fail to get the wallet info")
		return keysign.Response{
			Status: common.Fail,
			Blame:  tb.NewBlame(tb.InternalError, []tb.Node{}),
		}, errors.New("fail to get the wallet info")
	}
	threshold := int(walletInfo.Threshold) - 1
	blameMgr := keysignInstance.GetTssCommonStruct().GetBlameMgr()

	var receivedSig, generatedSig keysign.Response
	var errWait, errGen error
	sigChan := make(chan string, 2)
	wg := sync.WaitGroup{}
	wg.Add(2)
	keysignStartTime := time.Now()
	// we wait for signatures
	go func() {
		defer wg.Done()
		receivedSig, errWait = t.waitForSignatures(msgID, req.EncodedTx, req.Memo, walletAccessor, sigChan, threshold)
		// we received a valid signature indeed
		if errWait == nil {
			sigChan <- "signature received"
			t.logger.Info().Msgf("for message %s we get the signature from the peer", msgID)
			return
		}
		t.logger.Info().Msgf("we fail to get the valid signature with error %v", errWait)
	}()

	// we generate the signature ourselves
	go func() {
		defer wg.Done()
		generatedSig, errGen = t.generateSignature(msgID, req, threshold, localStateItem.ParticipantKeys, blameMgr, keysignInstance, sigChan)
	}()
	wg.Wait()
	close(sigChan)
	keysignTime := time.Since(keysignStartTime)
	// we received the generated verified signature, so we return
	if errWait == nil {
		t.updateKeySignResult(receivedSig, keysignTime)
		return receivedSig, nil
	}
	// for this round, we are not the active signer
	if errors.Is(errGen, p2p.ErrSignReceived) || errors.Is(errGen, p2p.ErrNotActiveSigner) {
		t.updateKeySignResult(receivedSig, keysignTime)
		return receivedSig, nil
	}
	// we get the signature from our tss keysign
	t.updateKeySignResult(generatedSig, keysignTime)
	return generatedSig, errGen
}

func (t *ServerImp) broadcastKeysignFailure(messageID string, peers []peer.ID) {
	if err := t.signatureNotifier.BroadcastFailed(messageID, peers); err != nil {
		t.logger.Err(err).Msg("fail to broadcast keysign failure")
	}
}
