package tss

import (
	"encoding/base64"
	"errors"
	"fmt"
	"sort"
	"sync"

	coskey "github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	sdk "github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tcrypto "github.com/tendermint/tendermint/crypto"
	ctss "gitlab.com/blackprotocol/tss/go-tss/common"
	"gitlab.com/blackprotocol/tss/go-tss/conversion"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"

	"gitlab.com/blackprotocol/tss/monero-tss/common"
	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig"
	moneroKeyGen "gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig/keygen"
	moneroKeySign "gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig/keysign"
	"gitlab.com/blackprotocol/tss/monero-tss/monitor"
	"gitlab.com/blackprotocol/tss/monero-tss/storage"
)

// ServerImp is the structure that can provide all keysign and key gen features
type ServerImp struct {
	conf                  common.TssConfig
	logger                zerolog.Logger
	p2pCommunication      *p2p.Communication
	localNodePubKey       string
	tssKeyGenLocker       *sync.Mutex
	stopChan              chan struct{}
	partyCoordinator      *p2p.PartyCoordinator
	stateManager          storage.LocalStateManager
	signatureNotifier     *moneroKeySign.SignatureNotifier
	privateKey            tcrypto.PrivKey
	tssMetrics            *monitor.Metric
	walletAccessorCreator monero_multi_sig.WalletAccessorCreator
}

// NewTss create a new instance of Tss
func NewTss(
	comm *p2p.Communication,
	priKey tcrypto.PrivKey,
	baseFolder string,
	conf common.TssConfig,
	walletAccessorCreator monero_multi_sig.WalletAccessorCreator,
	fileStateManagerCreator storage.LocalStateManagerCreator) (*ServerImp, error) {
	pk := coskey.PubKey{
		Key: priKey.PubKey().Bytes()[:],
	}

	pubKey, err := sdk.MarshalPubKey(sdk.AccPK, &pk)
	if err != nil {
		return nil, fmt.Errorf("fail to genearte the key: %w", err)
	}
	if fileStateManagerCreator == nil {
		return nil, fmt.Errorf("fileStateManagerCreator is nil")
	}
	stateManager, err := fileStateManagerCreator(baseFolder)
	if err != nil {
		return nil, fmt.Errorf("fail to create file state manager")
	}

	pc := p2p.NewPartyCoordinator(comm.GetHost(), conf.PartyTimeout, p2p.JoinPartyMNProtocolWithLeader)
	sn := moneroKeySign.NewSignatureNotifier(comm.GetHost())
	metrics := monitor.NewMetric()
	if conf.EnableMonitor {
		metrics.Enable()
	}
	tssServer := ServerImp{
		conf:                  conf,
		logger:                log.With().Str("module", "tss_mn").Logger(),
		p2pCommunication:      comm,
		localNodePubKey:       pubKey,
		tssKeyGenLocker:       &sync.Mutex{},
		stopChan:              make(chan struct{}),
		partyCoordinator:      pc,
		stateManager:          stateManager,
		signatureNotifier:     sn,
		privateKey:            priKey,
		tssMetrics:            metrics,
		walletAccessorCreator: walletAccessorCreator,
	}

	return &tssServer, nil
}

// Start Tss server
func (t *ServerImp) Start() error {
	log.Info().Msg("Starting the TSS servers")
	return nil
}

// Stop Tss server
func (t *ServerImp) Stop() {
	close(t.stopChan)
	// stop the p2p and finish the p2p wait group
	err := t.p2pCommunication.Stop()
	if err != nil {
		t.logger.Error().Msgf("error in shutdown the p2p server")
	}
	t.partyCoordinator.Stop()
	log.Info().Msg("The Tss and p2p server has been stopped successfully")
}

func (t *ServerImp) requestToMsgID(request any) (string, error) {
	var dat []byte
	var keys []string
	switch value := request.(type) {
	case moneroKeyGen.Request:
		keys = value.Keys
	case moneroKeySign.Request:
		msgToSign, err := base64.StdEncoding.DecodeString(value.EncodedTx)
		if err != nil {
			t.logger.Err(err).Msg("error in decode the keysign req")
			return "", err
		}
		dat = msgToSign
	default:
		t.logger.Error().Msg("unknown request type")
		return "", errors.New("unknown request type")
	}
	keyAccumulation := ""
	sort.Strings(keys)
	for _, el := range keys {
		keyAccumulation += el
	}
	dat = append(dat, []byte(keyAccumulation)...)
	return ctss.MsgToHashString(dat)
}

func (t *ServerImp) joinParty(msgID, _ string, blockHeight int64, participants []string, threshold int, sigChan chan string) ([]peer.ID, string, error) {
	t.logger.Info().Msg("apply join party with a leader")
	if len(participants) == 0 {
		t.logger.Error().Msg("fail to have any participants or passed by request")
		return nil, "", errors.New("no participants can be found")
	}
	peersID, err := conversion.GetPeerIDsFromPubKeys(participants)
	if err != nil {
		return nil, "", errors.New("fail to convert the public key to peer ID")
	}
	var peersIDStr []string
	for _, el := range peersID {
		peersIDStr = append(peersIDStr, el.String())
	}

	return t.partyCoordinator.JoinPartyWithLeader(msgID, blockHeight, peersIDStr, threshold, sigChan)
}

// GetLocalPeerID return the local peer
func (t *ServerImp) GetLocalPeerID() string {
	return t.p2pCommunication.GetLocalPeerID()
}
