package common

const (
	MoneroKeyGenSharepre      = "moneroKeyGenSharePre"
	MoneroKeyGenShareExchange = "moneroKeyGenShareExchange"
	MoneroExportedSignMsg     = "moneroExportedSignMsg"
	MoneroInitTransfer        = "moneroInitTrnasfer"
	MoneroSignShares          = "moneroSignShares"
)

type MoneroShare struct {
	MultisigInfo  string `json:"multisig_info"`
	MsgType       string `json:"message_type"`
	ExchangeRound int    `json:"exchangeRound"`
	// the sender field is only used for keysign
	Sender string `json:"message_sender"`
}

func (s *MoneroShare) Equal(in *MoneroShare) bool {
	return (in.Sender == s.Sender) && (s.MultisigInfo == in.MultisigInfo)
}
