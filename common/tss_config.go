package common

import (
	"time"
)

// TssConfig configurations
type TssConfig struct {
	// Party Timeout defines how long do we wait for the party to form
	PartyTimeout time.Duration
	// KeyGenTimeoutSeconds defines how long do we wait the keygen parties to pass messages along
	KeyGenTimeout time.Duration
	// KeySignTimeoutSeconds defines how long do we wait keysign
	KeySignTimeout time.Duration
	// enable the tss monitor
	EnableMonitor bool
	// tss need to access two monero wallet RPC , one for current active asgard , and one for retiring asgard
	// Mostly of the time it will only have active asgard , but during migration , it will have both retiring and active asgard
	// Monero wallet RPC endpoint - A
	WalletRPCA string
	// Monero wallet RPC endpoint - B
	WalletRPCB string
	// WalletPassword the password used to secure wallet
	WalletPassword string
}

// GetWalletRPC get the next wallet RPC url , we have two wallet RPC at the same time, the wallet hold active asgard
// can't be used to keygen
func (cfg TssConfig) GetWalletRPC(exclude string) string {
	if len(exclude) == 0 {
		return cfg.WalletRPCA
	}
	if exclude == cfg.WalletRPCA {
		return cfg.WalletRPCB
	} else if exclude == cfg.WalletRPCB {
		return cfg.WalletRPCA
	}
	return cfg.WalletRPCA
}
