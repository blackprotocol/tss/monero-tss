package common

import (
	"sync"

	"gitlab.com/blackprotocol/tss/go-tss/messages"
)

// LocalCacheItem used to cache the unconfirmed broadcast message
type LocalCacheItem struct {
	Msg           *messages.WireMessage
	Hash          string
	lock          *sync.Mutex
	ConfirmedList map[string]string
}

func NewLocalCacheItem(msg *messages.WireMessage, hash string) *LocalCacheItem {
	return &LocalCacheItem{
		Msg:           msg,
		Hash:          hash,
		lock:          &sync.Mutex{},
		ConfirmedList: make(map[string]string),
	}
}

// UpdateConfirmList add the given party's hash into the confirm list
func (l *LocalCacheItem) UpdateConfirmList(p2pID, hash string) {
	l.lock.Lock()
	defer l.lock.Unlock()
	l.ConfirmedList[p2pID] = hash
}

// TotalConfirmParty number of parties that already confirmed their hash
func (l *LocalCacheItem) TotalConfirmParty() int {
	l.lock.Lock()
	defer l.lock.Unlock()
	return len(l.ConfirmedList)
}

func (l *LocalCacheItem) GetPeers() []string {
	peers := make([]string, 0, len(l.ConfirmedList))
	l.lock.Lock()
	defer l.lock.Unlock()
	for peer := range l.ConfirmedList {
		peers = append(peers, peer)
	}
	return peers
}

// IsInConfirmedList check whether the given id is in confirmed list
func (l *LocalCacheItem) IsInConfirmedList(id string) bool {
	l.lock.Lock()
	defer l.lock.Unlock()
	_, ok := l.ConfirmedList[id]
	return ok
}

// RemovePeer remove the given peer from confirmed list
func (l *LocalCacheItem) RemovePeer(id string) {
	l.lock.Lock()
	defer l.lock.Unlock()
	delete(l.ConfirmedList, id)
}
