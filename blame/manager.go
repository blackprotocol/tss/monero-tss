package blame

import (
	"sync"

	btss "github.com/binance-chain/tss-lib/tss"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	b "gitlab.com/blackprotocol/tss/go-tss/blame"
)

// Manager deal with monero keygen / keysign related blame
type Manager struct {
	logger          zerolog.Logger
	blame           *b.Blame
	lastUnicastPeer map[string][]peer.ID
	shareMgr        *b.ShareMgr
	roundMgr        *b.RoundMgr
	partyInfo       *PartyInfo
	PartyIDtoP2PID  map[string]peer.ID
	lastMsgLocker   *sync.RWMutex
	lastMsg         string
	acceptedShares  *sync.Map
}

// NewBlameManager create a new instance of blame manager
func NewBlameManager() *Manager {
	blame := b.NewBlame("", nil)
	return &Manager{
		logger:          log.With().Str("module", "blame_manager_mn").Logger(),
		partyInfo:       nil,
		PartyIDtoP2PID:  make(map[string]peer.ID),
		lastUnicastPeer: make(map[string][]peer.ID),
		shareMgr:        b.NewTssShareMgr(),
		roundMgr:        b.NewTssRoundMgr(),
		blame:           &blame,
		lastMsgLocker:   &sync.RWMutex{},
		acceptedShares:  &sync.Map{},
	}
}

func (m *Manager) GetBlame() *b.Blame {
	return m.blame
}

func (m *Manager) GetShareMgr() *b.ShareMgr {
	return m.shareMgr
}

func (m *Manager) GetRoundMgr() *b.RoundMgr {
	return m.roundMgr
}

func (m *Manager) GetAcceptShares() *sync.Map {
	return m.acceptedShares
}

func (m *Manager) SetLastMsg(lastMsg string) {
	m.lastMsgLocker.Lock()
	defer m.lastMsgLocker.Unlock()
	m.lastMsg = lastMsg
}

func (m *Manager) GetLastMsg() string {
	m.lastMsgLocker.RLock()
	defer m.lastMsgLocker.RUnlock()
	return m.lastMsg
}

func (m *Manager) SetPartyInfo(party *btss.PartyID, partyIDMap map[string]*btss.PartyID) {
	partyInfo := &PartyInfo{
		Party:      party,
		PartyIDMap: partyIDMap,
	}
	m.partyInfo = partyInfo
}

func (m *Manager) SetLastUnicastPeer(peerID peer.ID, roundInfo string) {
	l, ok := m.lastUnicastPeer[roundInfo]
	if !ok {
		peerList := []peer.ID{peerID}
		m.lastUnicastPeer[roundInfo] = peerList
	} else {
		l = append(l, peerID)
		m.lastUnicastPeer[roundInfo] = l
	}
}
