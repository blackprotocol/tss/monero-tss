package storage

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFileStateMgr(t *testing.T) {
	m, err := NewFileStateMgr(os.TempDir())
	assert.Nil(t, err)
	assert.NotNil(t, m)
	stateForSave := WalletLocalState{
		Address:         "whatever-address",
		WalletName:      "wallet-name",
		ParticipantKeys: []string{"key1", "key2"},
	}
	assert.Nil(t, m.SaveLocalWalletState(stateForSave))
	savedState, err := m.GetLocalWalletState(stateForSave.Address)
	assert.Nil(t, err)
	assert.Equal(t, stateForSave, savedState)
}
