package storage

import (
	"github.com/stretchr/testify/mock"
)

// MockLocalStateManager mock local state manager
type MockLocalStateManager struct {
	mock.Mock
}

func (m *MockLocalStateManager) SaveLocalWalletState(state WalletLocalState) error {
	args := m.Called(state)
	return args.Error(0)
}

func (m *MockLocalStateManager) GetLocalWalletState(address string) (WalletLocalState, error) {
	args := m.Called(address)
	return args.Get(0).(WalletLocalState), args.Error(1)
}
