package storage

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"sync"
)

// WalletLocalState is a structure used to represent the data we saved locally for different keygen
type WalletLocalState struct {
	Address         string   `json:"address"`
	WalletName      string   `json:"wallet_name"`
	WalletRPC       string   `json:"wallet_rpc"`
	ParticipantKeys []string `json:"participant_keys"` // the paticipant of last key gen
}

// LocalStateManager provide necessary methods to manage the local state, save it , and read it back
// LocalStateManager doesn't have any opinion where it should be persistent to
type LocalStateManager interface {
	SaveLocalWalletState(state WalletLocalState) error
	GetLocalWalletState(address string) (WalletLocalState, error)
}

// LocalStateManagerCreator a function to create a new instance of FileStateMgr
type LocalStateManagerCreator func(folder string) (LocalStateManager, error)

// FileStateMgr save the local state to file
type FileStateMgr struct {
	folder    string
	writeLock *sync.RWMutex
}

// NewFileStateMgr create a new instance of the FileStateMgr which implements LocalStateManager
func NewFileStateMgr(folder string) (LocalStateManager, error) {
	if len(folder) > 0 {
		_, err := os.Stat(folder)
		if err != nil && os.IsNotExist(err) {
			if err := os.MkdirAll(folder, os.ModePerm); err != nil {
				return nil, err
			}
		}
	}
	return &FileStateMgr{
		folder:    folder,
		writeLock: &sync.RWMutex{},
	}, nil
}

func (fsm *FileStateMgr) getFilePathName(address string) (string, error) {
	localFileName := fmt.Sprintf("localstate-%s.json", address)
	if len(fsm.folder) > 0 {
		return filepath.Join(fsm.folder, localFileName), nil
	}
	return localFileName, nil
}

// SaveLocalWalletState save the local state to file
func (fsm *FileStateMgr) SaveLocalWalletState(state WalletLocalState) error {
	buf, err := json.Marshal(state)
	if err != nil {
		return fmt.Errorf("fail to marshal KeygenLocalState to json: %w", err)
	}
	filePathName, err := fsm.getFilePathName(state.Address)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filePathName, buf, 0o655)
}

// GetLocalWalletState read the local state from file system
func (fsm *FileStateMgr) GetLocalWalletState(address string) (WalletLocalState, error) {
	if len(address) == 0 {
		return WalletLocalState{}, errors.New("address is empty")
	}
	filePathName, err := fsm.getFilePathName(address)
	if err != nil {
		return WalletLocalState{}, err
	}
	if _, err := os.Stat(filePathName); os.IsNotExist(err) {
		return WalletLocalState{}, err
	}

	buf, err := os.ReadFile(filePathName)
	if err != nil {
		return WalletLocalState{}, fmt.Errorf("file to read from file(%s): %w", filePathName, err)
	}
	var localState WalletLocalState
	if err := json.Unmarshal(buf, &localState); nil != err {
		return WalletLocalState{}, fmt.Errorf("fail to unmarshal KeygenLocalState: %w", err)
	}
	return localState, nil
}

// GetLatestWalletState traverse the folder to find out the latest file
func (fsm *FileStateMgr) GetLatestWalletState() (WalletLocalState, error) {
	entries, err := os.ReadDir(fsm.folder)
	if err != nil {
		return WalletLocalState{}, fmt.Errorf("fail to read folder:%s,err:%w", fsm.folder, err)
	}
	var files []os.FileInfo
	for _, item := range entries {
		if item.IsDir() {
			continue
		}
		fileInfo, err := item.Info()
		if err != nil {
			return WalletLocalState{}, fmt.Errorf("fail to get file info,err:%w", err)
		}
		files = append(files, fileInfo)
	}
	sort.Slice(files, func(i, j int) bool {
		return files[i].ModTime().Before(files[j].ModTime())
	})
	targetFile := files[0]
	filePathName := filepath.Join(fsm.folder, targetFile.Name())
	buf, err := os.ReadFile(filePathName)
	if err != nil {
		return WalletLocalState{}, fmt.Errorf("file to read from file(%s): %w", filePathName, err)
	}
	var localState WalletLocalState
	if err := json.Unmarshal(buf, &localState); nil != err {
		return WalletLocalState{}, fmt.Errorf("fail to unmarshal KeygenLocalState: %w", err)
	}
	return localState, nil
}
