package monero_multi_sig

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

type MockWalletAccessor struct {
	mock.Mock
}

func (m *MockWalletAccessor) OpenWallet(req *wallet.RequestOpenWallet) error {
	args := m.Called(req)
	return args.Error(0)
}
func (m *MockWalletAccessor) IsMultisig() (*wallet.ResponseIsMultisig, error) {
	args := m.Called()
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseIsMultisig), args.Error(1)
}
func (m *MockWalletAccessor) PrepareMultisig() (*wallet.ResponsePrepareMultisig, error) {
	args := m.Called()
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponsePrepareMultisig), args.Error(1)
}
func (m *MockWalletAccessor) MakeMultisig(req *wallet.RequestMakeMultisig) (*wallet.ResponseMakeMultisig, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseMakeMultisig), args.Error(1)
}
func (m *MockWalletAccessor) ExportMultisigInfo() (*wallet.ResponseExportMultisigInfo, error) {
	args := m.Called()
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseExportMultisigInfo), args.Error(1)
}
func (m *MockWalletAccessor) ImportMultisigInfo(req *wallet.RequestImportMultisigInfo) (*wallet.ResponseImportMultisigInfo, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseImportMultisigInfo), args.Error(1)
}

func (m *MockWalletAccessor) SubmitMultisig(req *wallet.RequestSubmitMultisig) (*wallet.ResponseSubmitMultisig, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseSubmitMultisig), args.Error(1)
}
func (m *MockWalletAccessor) GetAddress(req *wallet.RequestGetAddress) (*wallet.ResponseGetAddress, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseGetAddress), args.Error(1)
}

func (m *MockWalletAccessor) GetTxProof(req *wallet.RequestGetTxProof) (*wallet.ResponseGetTxProof, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseGetTxProof), args.Error(1)
}
func (m *MockWalletAccessor) CheckTxProof(req *wallet.RequestCheckTxProof) (*wallet.ResponseCheckTxProof, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseCheckTxProof), args.Error(1)
}
func (m *MockWalletAccessor) Refresh(req *wallet.RequestRefresh) (*wallet.ResponseRefresh, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseRefresh), args.Error(1)
}
func (m *MockWalletAccessor) CreateWallet(req *wallet.RequestCreateWallet) error {
	args := m.Called(req)
	return args.Error(0)
}
func (m *MockWalletAccessor) GetBalance(req *wallet.RequestGetBalance) (*wallet.ResponseGetBalance, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseGetBalance), args.Error(1)
}
func (m *MockWalletAccessor) Transfer(req *wallet.RequestTransfer) (*wallet.ResponseTransfer, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseTransfer), args.Error(1)
}
func (m *MockWalletAccessor) CheckTransaction(req *wallet.RequestCheckTransaction) (*wallet.ResponseCheckTransaction, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseCheckTransaction), args.Error(1)
}
func (m *MockWalletAccessor) SignMultisig(req *wallet.RequestSignMultisig) (*wallet.ResponseSignMultisig, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseSignMultisig), args.Error(1)
}
func (m *MockWalletAccessor) ExchangeMultiSigKeys(req *wallet.RequestExchangeMultisigKeys) (*wallet.ResponseExchangeMultisig, error) {
	args := m.Called(req)
	resp := args.Get(0)
	if resp == nil {
		return nil, args.Error(1)
	}
	return resp.(*wallet.ResponseExchangeMultisig), args.Error(1)
}
