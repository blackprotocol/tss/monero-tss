package monero_multi_sig

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"sync"

	tcrypto "github.com/tendermint/tendermint/crypto"
	"github.com/tendermint/tendermint/crypto/secp256k1"

	"gitlab.com/blackprotocol/tss/monero-tss/common"
)

type MoneroSharesStore struct {
	shares map[int][]*common.MoneroShare
	locker sync.Mutex
}

type MoneroPrepareMsg struct {
	ExchangeInfo string `json:"exchange_info"`
}

func GenMoneroShareStore() *MoneroSharesStore {
	shares := make(map[int][]*common.MoneroShare)
	return &MoneroSharesStore{
		shares,
		sync.Mutex{},
	}
}

func (ms *MoneroSharesStore) StoreAndCheck(round int, share *common.MoneroShare, checkLength int) ([]*common.MoneroShare, bool) {
	ms.locker.Lock()
	defer ms.locker.Unlock()
	shares, ok := ms.shares[round]
	if ok {
		for _, el := range shares {
			if el.Equal(share) {
				return nil, false
			}
		}

		shares = append(shares, share)
		ms.shares[round] = shares
	} else {
		ms.shares[round] = []*common.MoneroShare{share}
	}

	if len(ms.shares[round]) == checkLength {
		return ms.shares[round], true
	}

	return ms.shares[round], false
}

// EncodePrepareInfo json marshal the given exported mutisiginfo and then base64 encode it
func EncodePrepareInfo(exportedMultiSigInfo string) (string, error) {
	out, err := json.Marshal(MoneroPrepareMsg{
		ExchangeInfo: exportedMultiSigInfo,
	})
	if err != nil {
		return "", fmt.Errorf("fail to marshal MoneroPrepareMsg,err: %w", err)
	}
	return base64.StdEncoding.EncodeToString(out), nil
}

// DecodePrepareInfo reverse of EncodePrepareInfo , base64 decode the input
// and then unmarshal it to MoneroPrepareMsg
func DecodePrepareInfo(in string) (MoneroPrepareMsg, error) {
	var dat MoneroPrepareMsg
	out, err := base64.StdEncoding.DecodeString(in)
	if err != nil {
		return dat, fmt.Errorf("fail to base64 decode the given information,err: %w", err)
	}
	err = json.Unmarshal(out, &dat)
	if err != nil {
		return dat, fmt.Errorf("fail to unmarshal input,err: %w", err)
	}
	return dat, err
}

// GetNodePrivateKey parse encode key to an instance of PrivKey
func GetNodePrivateKey(encodedKey string) (tcrypto.PrivKey, error) {
	priHexBytes, err := base64.StdEncoding.DecodeString(encodedKey)
	if err != nil {
		return nil, fmt.Errorf("fail to base64 decode key,err: %w", err)
	}
	rawBytes, err := hex.DecodeString(string(priHexBytes))
	if err != nil {
		return nil, fmt.Errorf("fail to hex decode private key,err: %w", err)
	}
	var priKey secp256k1.PrivKey
	priKey = rawBytes[:32]
	return priKey, nil
}
