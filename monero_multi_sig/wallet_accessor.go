package monero_multi_sig

import (
	mr "gitlab.com/blackprotocol/monero-rpc"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

// WalletAccessor contains all the methods monero tss need to use to finish keygen and keysign
type WalletAccessor interface {
	OpenWallet(*wallet.RequestOpenWallet) error
	IsMultisig() (*wallet.ResponseIsMultisig, error)
	PrepareMultisig() (*wallet.ResponsePrepareMultisig, error)
	MakeMultisig(*wallet.RequestMakeMultisig) (*wallet.ResponseMakeMultisig, error)
	ExportMultisigInfo() (*wallet.ResponseExportMultisigInfo, error)
	ImportMultisigInfo(*wallet.RequestImportMultisigInfo) (*wallet.ResponseImportMultisigInfo, error)
	SubmitMultisig(*wallet.RequestSubmitMultisig) (*wallet.ResponseSubmitMultisig, error)
	GetAddress(*wallet.RequestGetAddress) (*wallet.ResponseGetAddress, error)
	GetTxProof(*wallet.RequestGetTxProof) (*wallet.ResponseGetTxProof, error)
	CheckTxProof(*wallet.RequestCheckTxProof) (*wallet.ResponseCheckTxProof, error)
	Refresh(*wallet.RequestRefresh) (*wallet.ResponseRefresh, error)
	CreateWallet(*wallet.RequestCreateWallet) error
	GetBalance(*wallet.RequestGetBalance) (*wallet.ResponseGetBalance, error)
	Transfer(*wallet.RequestTransfer) (*wallet.ResponseTransfer, error)
	CheckTransaction(req *wallet.RequestCheckTransaction) (*wallet.ResponseCheckTransaction, error)
	SignMultisig(req *wallet.RequestSignMultisig) (*wallet.ResponseSignMultisig, error)
	ExchangeMultiSigKeys(*wallet.RequestExchangeMultisigKeys) (*wallet.ResponseExchangeMultisig, error)
}

// WalletAccessorCreator function to create a WalletAccessor
type WalletAccessorCreator func(rpc string) WalletAccessor

// DefaultWalletAccessorCreator default method to create a wallet client
func DefaultWalletAccessorCreator(rpc string) WalletAccessor {
	return wallet.New(mr.Config{
		Address: rpc,
	})
}
