package keygen

import (
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"

	"gitlab.com/blackprotocol/tss/go-tss/common"
)

// Response keygen response
type Response struct {
	PoolAddress string        `json:"pool_address"`
	Status      common.Status `json:"status"`
	Blame       tb.Blame      `json:"blame"`
}

// NewResponse create a new instance of keygen.Response
func NewResponse(addr string, status common.Status, blame tb.Blame) Response {
	return Response{
		PoolAddress: addr,
		Status:      status,
		Blame:       blame,
	}
}
