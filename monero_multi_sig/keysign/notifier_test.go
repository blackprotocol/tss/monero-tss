package keysign

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	mp "gitlab.com/blackprotocol/monero-rpc"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

var (
	testEncodedTransaction = "eyJkZXN0aW5hdGlvbnMiOlt7ImFtb3VudCI6NTAwLCJhZGRyZXNzIjoiNDhRcDFEWVk5NXdGMkJOYmhRWkRkNUo4ZFpDdWNNUno5OVk0d0FVYURqUWhqWDhyb3lvd2ZvZzFzTjlXQWRWZXNoUXV2VTZxS0ZpOUppNGdqOVpSRWtqVEZZc1FiWlgifV0sImFjY291bnRfaW5kZXgiOjAsInN1YmFkZHJfaW5kaWNlcyI6bnVsbCwicHJpb3JpdHkiOjAsIm1peGluIjowLCJyaW5nX3NpemUiOjExLCJ1bmxvY2tfdGltZSI6MCwicGF5bWVudF9pZCI6IiIsImdldF90eF9rZXkiOnRydWUsImdldF90eF9oZXgiOnRydWUsImdldF90eF9tZXRhZGF0YSI6dHJ1ZX0="
)

func TestNewNotifier(t *testing.T) {
	config := mp.Config{
		Address: "fake",
	}
	dummyClient := wallet.New(config)
	n, err := NewNotifier("", testEncodedTransaction, "", dummyClient, 0)
	assert.NotNil(t, err, "when message id is empty , expect an error")
	assert.Nil(t, n)
	n, err = NewNotifier("aasfdasdf", testEncodedTransaction, "", nil, 1)
	assert.NotNil(t, err, "when txProofChecker is nil , it should error")
	assert.Nil(t, n)
	n, err = NewNotifier("aasfdasdf", "", "", dummyClient, 1)
	assert.NotNil(t, err, "when encodedTx is empty , expect an error")
	assert.Nil(t, n)
	n, err = NewNotifier("hello", "aaaa", "", dummyClient, 1)
	assert.NotNil(t, err, "encodeTx can't be decoded")
	assert.Nil(t, n)
	n, err = NewNotifier("hello", testEncodedTransaction, "", dummyClient, 0)
	assert.NotNil(t, err, "threshold can't be 0")
	assert.Nil(t, n)
	n, err = NewNotifier("hello", testEncodedTransaction, "", dummyClient, 1)
	assert.Nil(t, err)
	assert.NotNil(t, n)
	ch := n.GetResponseChannel()
	assert.NotNil(t, ch, "response channel should not be nil")
}

func TestNotifierHappyPath(t *testing.T) {
	txProofChecker := new(MockTxProofChecker)
	n, err := NewNotifier("happy path test", testEncodedTransaction, "", txProofChecker, 1)
	assert.Nil(t, err)
	assert.NotNil(t, n)
	mockCall := txProofChecker.On("CheckTxProof", mock.Anything).Return(&wallet.ResponseCheckTxProof{
		Confirmations: 0,
		Good:          true,
		InPool:        false,
		Received:      0,
	}, nil)
	result, err := n.ProcessSignature(nil)
	assert.Nil(t, err)
	assert.False(t, result)

	result, err = n.ProcessSignature(&TxSendProof{
		TransactionID: "whatever",
		Signature:     "",
	})
	assert.Nil(t, err)
	assert.False(t, result)

	result, err = n.ProcessSignature(&TxSendProof{
		TransactionID: "",
		Signature:     "signature",
	})
	assert.Nil(t, err)
	assert.False(t, result)

	result, err = n.ProcessSignature(&TxSendProof{
		TransactionID: "whatever",
		Signature:     "signature",
	})
	assert.Nil(t, err)
	assert.True(t, result)
	assert.Len(t, n.GetResponseChannel(), 1)

	// when signature can't be verified , it should return error
	invalidProof := &wallet.RequestCheckTxProof{
		TxID:      "whatever",
		Address:   "48Qp1DYY95wF2BNbhQZDd5J8dZCucMRz99Y4wAUaDjQhjX8royowfog1sN9WAdVeshQuvU6qKFi9Ji4gj9ZREkjTFYsQbZX",
		Message:   "",
		Signature: "invalid signature",
	}
	mockCall.Unset()

	mockCall = txProofChecker.On("CheckTxProof", invalidProof).Return(&wallet.ResponseCheckTxProof{
		Good: false,
	}, nil)

	result, err = n.ProcessSignature(&TxSendProof{
		TransactionID: "whatever",
		Signature:     "invalid signature",
	})
	assert.False(t, result)
	assert.NotNil(t, err)
	mockCall.Unset()

	mockCall = txProofChecker.On("CheckTxProof", mock.Anything).Return(&wallet.ResponseCheckTxProof{
		Good: false,
	}, errors.New("failed to verify signature"))

	result, err = n.ProcessSignature(&TxSendProof{
		TransactionID: "whatever",
		Signature:     "invalid signature",
	})
	assert.False(t, result)
	assert.NotNil(t, err)
}
