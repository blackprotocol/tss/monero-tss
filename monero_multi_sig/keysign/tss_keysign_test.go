package keysign

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/blackprotocol/tss/go-tss/messages"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"

	"gitlab.com/blackprotocol/tss/monero-tss/common"
	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig"
)

var (
	testNodePrivkey = []string{
		"Zjc1NGI1YzBiOTIwZjVlYzNlMzY0YjFiZjMwOTQ3ZGQ4YTg0ZWNjYzVjZjE0YmQwN2E2ZmQyMjdkMGQzNmYyNQ==",
	}
)

func TestNewMoneroKeySign(t *testing.T) {
	broadcastChan := make(chan *messages.BroadcastMsgChan)
	nodePrivateKey, err := monero_multi_sig.GetNodePrivateKey(testNodePrivkey[0])
	assert.Nil(t, err)
	comm, err := p2p.NewCommunication("asgard", nil, 6060, "", p2p.TSSMNProtocolID)
	assert.Nil(t, err)
	walletAccessor := new(monero_multi_sig.MockWalletAccessor)
	walletAccessor.On("OpenWallet", mock.Anything).Return(nil)
	ksInstance, err := NewMoneroKeySign("localP2PID", common.TssConfig{
		PartyTimeout:   0,
		KeyGenTimeout:  0,
		KeySignTimeout: 0,
		EnableMonitor:  false,
		WalletRPCA:     "http://localhost",
		WalletRPCB:     "http://localhost",
	}, broadcastChan, make(chan struct{}), "msgid", nodePrivateKey, comm, walletAccessor, "walletName")
	assert.Nil(t, err)
	assert.NotNil(t, ksInstance)
}
