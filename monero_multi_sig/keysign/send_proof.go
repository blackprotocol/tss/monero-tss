package keysign

// TxSendProof information can be used to proof the network sent the outbound
type TxSendProof struct {
	TransactionID string
	Signature     string
}

// IsEmpty check whether TxSendProof is empty
func (t TxSendProof) IsEmpty() bool {
	return t.TransactionID == "" && t.Signature == ""
}
