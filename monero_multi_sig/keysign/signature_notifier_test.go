package keysign

import (
	"sync"
	"testing"
	"time"

	tnet "github.com/libp2p/go-libp2p-testing/net"
	"github.com/libp2p/go-libp2p/core/peer"
	mocknet "github.com/libp2p/go-libp2p/p2p/net/mock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
	"gitlab.com/blackprotocol/tss/go-tss/conversion"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"
)

func TestSignatureNotifierHappyPath(t *testing.T) {
	conversion.SetupBech32Prefix()
	p2p.ApplyDeadline = false
	id1 := tnet.RandIdentityOrFatal(t)
	id2 := tnet.RandIdentityOrFatal(t)
	id3 := tnet.RandIdentityOrFatal(t)
	mn := mocknet.New()
	// add peers to mock net

	a1 := tnet.RandLocalTCPAddress()
	a2 := tnet.RandLocalTCPAddress()
	a3 := tnet.RandLocalTCPAddress()

	h1, err := mn.AddPeer(id1.PrivateKey(), a1)
	assert.Nil(t, err)

	p1 := h1.ID()
	h2, err := mn.AddPeer(id2.PrivateKey(), a2)
	assert.Nil(t, err)

	p2 := h2.ID()
	h3, err := mn.AddPeer(id3.PrivateKey(), a3)
	assert.Nil(t, err)

	p3 := h3.ID()
	err = mn.LinkAll()
	assert.Nil(t, err)

	err = mn.ConnectAllButSelf()
	assert.Nil(t, err)

	n1 := NewSignatureNotifier(h1)
	n2 := NewSignatureNotifier(h2)
	n3 := NewSignatureNotifier(h3)
	assert.NotNil(t, n1)
	assert.NotNil(t, n2)
	assert.NotNil(t, n3)

	messageID := "testMessage"

	spendProof := TxSendProof{
		TransactionID: "6283f00c65ddf91e3f28439f437abf983039284468fefaeaa16ecb6cd7492205",
		Signature:     "549d0034f7799eecf7531d5311a3c8fee08eacc1fe964e791973a958d175b87a",
	}
	txProofChecker := new(MockTxProofChecker)
	txProofChecker.On("CheckTxProof", mock.Anything).Return(&wallet.ResponseCheckTxProof{
		Good: true,
	}, nil)
	sigChan := make(chan string)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		sig, err := n1.WaitForSignature(messageID, testEncodedTransaction, "test memo", txProofChecker, time.Second*30, sigChan, 2)
		assert.Nil(t, err)
		assert.NotNil(t, sig)
	}()

	assert.Nil(t, n2.BroadcastSignature(messageID, &spendProof, []peer.ID{
		p1, p3,
	}))
	assert.Nil(t, n3.BroadcastSignature(messageID, &spendProof, []peer.ID{
		p1, p2,
	}))
	wg.Wait()
	txProofChecker.AssertExpectations(t)
}
