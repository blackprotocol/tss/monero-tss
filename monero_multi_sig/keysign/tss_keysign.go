package keysign

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	btss "github.com/binance-chain/tss-lib/tss"
	coskey "github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	sdk "github.com/cosmos/cosmos-sdk/types/bech32/legacybech32"
	set "github.com/deckarep/golang-set"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tcrypto "github.com/tendermint/tendermint/crypto"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"
	"gitlab.com/blackprotocol/tss/go-tss/conversion"
	"gitlab.com/blackprotocol/tss/go-tss/messages"
	"gitlab.com/blackprotocol/tss/go-tss/p2p"
	"gitlab.com/blackprotocol/tss/go-tss/storage"

	"gitlab.com/blackprotocol/tss/monero-tss/common"
	"gitlab.com/blackprotocol/tss/monero-tss/monero_multi_sig"
)

// MoneroKeySign structure to process monero keysign
type MoneroKeySign struct {
	logger             zerolog.Logger
	moneroCommonStruct *common.TssCommon
	localNodePubKey    string
	stopChan           chan struct{} // channel to indicate whether we should stop
	localParty         *btss.PartyID
	commStopChan       chan struct{}
	p2pComm            *p2p.Communication
	stateManager       storage.LocalStateManager
	walletAccessor     monero_multi_sig.WalletAccessor
}

// NewMoneroKeySign create a new instance of Monero Keysign
func NewMoneroKeySign(localP2PID string, conf common.TssConfig,
	broadcastChan chan *messages.BroadcastMsgChan,
	stopChan chan struct{},
	msgID string,
	privKey tcrypto.PrivKey,
	p2pComm *p2p.Communication,
	walletAccessor monero_multi_sig.WalletAccessor,
	walletName string) (*MoneroKeySign, error) {
	pk := coskey.PubKey{
		Key: privKey.PubKey().Bytes(),
	}
	bech32PubKey, _ := sdk.MarshalPubKey(sdk.AccPK, &pk)

	moneroSignClient := MoneroKeySign{
		logger:             log.With().Str("module", "monero_keysing").Str("message_id", msgID).Logger(),
		localNodePubKey:    bech32PubKey,
		moneroCommonStruct: common.NewTssCommon(localP2PID, broadcastChan, conf, msgID, privKey),
		stopChan:           stopChan,
		localParty:         nil,
		commStopChan:       make(chan struct{}),
		p2pComm:            p2pComm,
		walletAccessor:     walletAccessor,
	}

	// now open the wallet
	walletOpenReq := wallet.RequestOpenWallet{
		Filename: walletName,
		Password: conf.WalletPassword,
	}
	err := moneroSignClient.walletAccessor.OpenWallet(&walletOpenReq)
	if err != nil {
		return nil, fmt.Errorf("fail to open wallet,err: %w", err)
	}

	return &moneroSignClient, nil
}

// GetTssKeySignChannels return the tss message channel
func (tKeySign *MoneroKeySign) GetTssKeySignChannels() chan *p2p.Message {
	return tKeySign.moneroCommonStruct.TssMsg
}

// GetTssCommonStruct return the common struct used by keysign
func (tKeySign *MoneroKeySign) GetTssCommonStruct() *common.TssCommon {
	return tKeySign.moneroCommonStruct
}

// OrderKeysignMembers sort keysign members, based on message id and member's node public key
func (tKeySign *MoneroKeySign) OrderKeysignMembers(msgID string, parties []string) []string {
	hashes := make([]string, len(parties))
	hashesToPubKeys := make(map[string]string)
	for i, el := range parties {
		sum := sha256.Sum256([]byte(msgID + el))
		encodedSum := hex.EncodeToString(sum[:])
		hashes[i] = encodedSum
		hashesToPubKeys[encodedSum] = el
	}
	sort.Strings(hashes)
	result := make([]string, len(parties))
	for idx, item := range hashes {
		result[idx] = hashesToPubKeys[item]
	}
	return result
}

func (tKeySign *MoneroKeySign) packAndSend(info string, exchangeRound int, localPartyID, toParty *btss.PartyID, msgType string) error {
	sendShare := common.MoneroShare{
		MultisigInfo:  info,
		MsgType:       msgType,
		ExchangeRound: exchangeRound,
	}
	msg, err := json.Marshal(sendShare)
	if err != nil {
		return fmt.Errorf("fail to encode wallet share,err: %w", err)
	}
	roundInfo := msgType + strconv.FormatInt(int64(exchangeRound), 10)
	tKeySign.moneroCommonStruct.GetBlameMgr().SetLastMsg(roundInfo)
	if toParty == nil {
		r := btss.MessageRouting{
			From:        localPartyID,
			IsBroadcast: true,
		}
		return tKeySign.moneroCommonStruct.ProcessOutCh(msg, &r, roundInfo, messages.TSSKeySignMsg)
	}
	r := btss.MessageRouting{
		From:        localPartyID,
		To:          []*btss.PartyID{toParty},
		IsBroadcast: false,
	}
	return tKeySign.moneroCommonStruct.ProcessOutCh(msg, &r, roundInfo, messages.TSSKeySignMsg)
}

func (tKeySign *MoneroKeySign) submitSignature(signature string) ([]string, error) {
	client2Submit := wallet.RequestSubmitMultisig{
		TxDataHex: signature,
	}
	signedTxHash, err := tKeySign.walletAccessor.SubmitMultisig(&client2Submit)
	return signedTxHash.TxHashList, err
}

func (tKeySign *MoneroKeySign) processPrepareMsg(shares []*common.MoneroShare) (map[string]monero_multi_sig.MoneroPrepareMsg, error) {
	// we store the peer's public keys
	peerPrepareMsg := make(map[string]monero_multi_sig.MoneroPrepareMsg)
	for _, el := range shares {
		dat, err := monero_multi_sig.DecodePrepareInfo(el.MultisigInfo)
		if err != nil {
			return nil, err
		}
		peerPrepareMsg[el.Sender] = dat
	}

	return peerPrepareMsg, nil
}

func (tKeySign *MoneroKeySign) calculatePubkeyForUse(pubkeysAll map[string][]string, keys *wallet.ResponseExportSigPubkey, orderedNodes []string) []string {
	var nodesBeforeMe []string
	for _, el := range orderedNodes {
		if el == tKeySign.localNodePubKey {
			break
		}
		nodesBeforeMe = append(nodesBeforeMe, el)
	}

	// now we figure out the public key that I will use.
	peerPubKeys := set.NewSet()
	for _, el := range nodesBeforeMe {
		pubKeys := pubkeysAll[el]
		for _, pk := range pubKeys {
			peerPubKeys.Add(pk)
		}
	}

	myPubKey := set.NewSet()
	for _, el := range keys.PubKeys {
		myPubKey.Add(el)
	}

	selectedKeys := myPubKey.Difference(peerPubKeys)
	var rselectedKeys []string
	for _, el := range selectedKeys.ToSlice() {
		rselectedKeys = append(rselectedKeys, el.(string))
	}

	return rselectedKeys
}

func (tKeySign *MoneroKeySign) submitAndGetConfirm(txForSubmit string, destination, memo string) (TxSendProof, error) {
	submitResult, err := tKeySign.walletAccessor.SubmitMultisig(&wallet.RequestSubmitMultisig{
		TxDataHex: txForSubmit,
	})
	if err != nil {
		return TxSendProof{}, fmt.Errorf("fail to submit signature,err: %w", err)
	}
	signedTx := TxSendProof{
		TransactionID: submitResult.TxHashList[0],
		Signature:     "",
	}
	proofResult, err := tKeySign.walletAccessor.GetTxProof(&wallet.RequestGetTxProof{
		TxID:    submitResult.TxHashList[0],
		Address: destination,
		Message: memo,
	})
	if err != nil {
		return signedTx, fmt.Errorf("fail to get proof")
	}
	signedTx.Signature = proofResult.Signature
	return signedTx, nil
}

func getPartyIDToPeerMap(parties map[string]*btss.PartyID) (map[string]peer.ID, error) {
	result := make(map[string]peer.ID)
	for id, party := range parties {
		peerID, err := conversion.GetPeerIDFromPartyID(party)
		if err != nil {
			return nil, err
		}
		result[id] = peerID
	}
	return result, nil
}

func (tKeySign *MoneroKeySign) getNextSigningParty(localNodePubKey string, sortedNodes []string, allParties []*btss.PartyID) (*btss.PartyID, error) {
	localNodeIdx := -1
	for idx, item := range sortedNodes {
		if item == localNodePubKey {
			localNodeIdx = idx
			break
		}
	}
	if localNodeIdx == -1 {
		return nil, errors.New("fail to get next keysign party")
	}
	localNodeIdx++
	if localNodeIdx >= len(sortedNodes) {
		return nil, errors.New("fail to get next keysign party")
	}
	nextNodePubKey := sortedNodes[localNodeIdx]
	pk, err := sdk.UnmarshalPubKey(sdk.AccPK, nextNodePubKey)
	if err != nil {
		return nil, fmt.Errorf("fail to decode next node pubkey,err: %w", err)
	}
	for _, item := range allParties {
		if bytes.Equal(item.Key, pk.Bytes()) {
			return item, nil
		}
	}
	return nil, errors.New("fail to get next keysign party")
}

// SignMessage use multisig to process the given encoded Tx
// parties represent all the members of the multisig wallet.
func (tKeySign *MoneroKeySign) SignMessage(encodedTx string, parties []string, memo string) (*TxSendProof, error) {
	var globalErr error
	partiesID, localPartyID, err := conversion.GetParties(parties, tKeySign.localNodePubKey)
	tKeySign.localParty = localPartyID
	if err != nil {
		return nil, fmt.Errorf("fail to form key sign party: %w", err)
	}
	tKeySign.logger.Debug().Msgf("local party: %+v", localPartyID)
	blameMgr := tKeySign.moneroCommonStruct.GetBlameMgr()
	partyIDMap := conversion.SetupPartyIDMap(partiesID)
	partyIDToP2PID, err := getPartyIDToPeerMap(partyIDMap)
	if err != nil {
		return nil, fmt.Errorf("fail to get party id to peers map,err: %w", err)
	}
	tKeySign.moneroCommonStruct.PartyIDtoP2PID = partyIDToP2PID
	blameMgr.PartyIDtoP2PID = partyIDToP2PID

	tKeySign.moneroCommonStruct.SetPartyInfo(&common.PartyInfo{
		Party:      localPartyID,
		PartyIDMap: partyIDMap,
	})
	blameMgr.SetPartyInfo(localPartyID, partyIDMap)
	// populate P2P Peers , exclude itself
	tKeySign.moneroCommonStruct.P2PPeers = conversion.GetPeersID(tKeySign.moneroCommonStruct.PartyIDtoP2PID, tKeySign.moneroCommonStruct.GetLocalPeerID())
	var keySignWg sync.WaitGroup

	walletInfo, err := tKeySign.walletAccessor.IsMultisig()
	if err != nil {
		return nil, fmt.Errorf("fail to query wallet info,err: %w", err)
	}
	if !walletInfo.Multisig || !walletInfo.Ready {
		return nil, errors.New("not a multisig wallet or wallet is not ready(keygen done correctly?)")
	}

	tx, err := base64.StdEncoding.DecodeString(encodedTx)
	if err != nil {
		return nil, fmt.Errorf("fail to decode transaction,err: %w", err)
	}

	var txSend wallet.RequestTransfer
	if err := json.Unmarshal(tx, &txSend); err != nil {
		return nil, fmt.Errorf("fail to unmarshal transafer,err: %w", err)
	}

	threshold := walletInfo.Threshold
	needToWait := threshold - 1 // we do not need to wait for ourselves

	// import message
	orderedNodes := tKeySign.OrderKeysignMembers(tKeySign.GetTssCommonStruct().GetMsgID(), parties)
	leader := orderedNodes[0]
	tKeySign.logger.Debug().Msgf("leader address: %s", leader)

	isLeader := leader == tKeySign.localNodePubKey

	moneroShareChan := make(chan *common.MoneroShare, len(partiesID))
	keySignWg.Add(1)
	go tKeySign.moneroCommonStruct.ProcessInboundmessages(tKeySign.commStopChan, &keySignWg, moneroShareChan)
	// TODO: this might be a bad idea , need to test it out when on mainnet
	refreshResp, err := tKeySign.walletAccessor.Refresh(&wallet.RequestRefresh{})
	if err != nil {
		return nil, fmt.Errorf("fail to request wallet to do a refresh")
	}
	tKeySign.logger.Debug().Msgf("refresh response: %+v", refreshResp)
	// Exchange the keysign preparation info
	exportedMultisigInfo, err := tKeySign.walletAccessor.ExportMultisigInfo()
	if err != nil {
		return nil, fmt.Errorf("fail to export multisign info,err : %w", err)
	}

	encodedMsg, err := monero_multi_sig.EncodePrepareInfo(exportedMultisigInfo.Info)
	if err != nil {
		return nil, fmt.Errorf("fail to encode prepare information,err: %w", err)
	}

	err = tKeySign.packAndSend(encodedMsg, 0, localPartyID, nil, common.MoneroExportedSignMsg)
	if err != nil {
		return nil, fmt.Errorf("fail to send MoneroExportedSignMsg,err: %w", err)
	}

	shareStore := monero_multi_sig.GenMoneroShareStore()
	tssConf := tKeySign.moneroCommonStruct.GetConf()
	var signedTx TxSendProof
	keySignWg.Add(1)
	go func() {
		defer func() {
			keySignWg.Done()
			close(tKeySign.commStopChan)
		}()
		notifyKeySignFinishedWithError := func() {
			if err := tKeySign.moneroCommonStruct.NotifyTaskDone(); err != nil {
				tKeySign.logger.Err(err).Msg("fail to broadcast keysign done with error")
			}
		}
		for {
			select {
			case <-time.After(tssConf.KeySignTimeout):
				tKeySign.logger.Error().Msgf("fail to generate the signature with %v", tssConf.KeySignTimeout)
				globalErr = tb.ErrTssTimeOut
				return

			case share := <-moneroShareChan:
				switch share.MsgType {
				case common.MoneroExportedSignMsg:
					shares, ready := shareStore.StoreAndCheck(0, share, int(needToWait))
					if !ready {
						continue
					}

					peerPreparedMsg, err := tKeySign.processPrepareMsg(shares)
					if err != nil {
						globalErr = err
						tKeySign.logger.Err(err).Msg("fail to process keysign prepare message")
						notifyKeySignFinishedWithError()
						return
					}
					var multiSigInfo []string
					for _, dat := range peerPreparedMsg {
						multiSigInfo = append(multiSigInfo, dat.ExchangeInfo)
					}

					info := wallet.RequestImportMultisigInfo{
						Info: multiSigInfo,
					}
					resp, err := tKeySign.walletAccessor.ImportMultisigInfo(&info)
					if err != nil {
						tKeySign.logger.Err(err).Msg("fail to import the multisig info")
						globalErr = err
						notifyKeySignFinishedWithError()
						return
					}
					tKeySign.logger.Debug().Msgf("response from import multisig info: %+v", resp)

					// if we are the leader, we need to initialise the wallet.
					if isLeader {
						tKeySign.logger.Info().Msgf("leader initiate transfer, destination: %s,amount: %d", txSend.Destinations[0].Address, txSend.Destinations[0].Amount)
						responseTransfer, err := tKeySign.walletAccessor.Transfer(&txSend)
						if err != nil {
							tKeySign.logger.Err(err).
								Str("node_pubkey", tKeySign.localNodePubKey).
								Msg("fail to request transfer ")
							globalErr = err
							notifyKeySignFinishedWithError()
							return
						}
						tKeySign.logger.Debug().Msgf("leader share: %s", responseTransfer.MultisigTxset)
						nextParty, err := tKeySign.getNextSigningParty(tKeySign.localNodePubKey, orderedNodes, partiesID)
						if err != nil {
							tKeySign.logger.Err(err).Msg("fail to find next signing party")
							globalErr = err
							notifyKeySignFinishedWithError()
							return
						}
						if nextParty == nil {
							globalErr = errors.New("can't find next signing party")
							notifyKeySignFinishedWithError()
							return
						}
						err = tKeySign.packAndSend(responseTransfer.MultisigTxset, 1, localPartyID, nextParty, common.MoneroSignShares)
						if err != nil {
							tKeySign.logger.Err(err).Msg("fail to send the initialization transfer info")
							globalErr = err
							notifyKeySignFinishedWithError()
							return
						}
						tKeySign.logger.Info().Msg("leader finished signature preparation")
					} else {
						tKeySign.logger.Info().
							Str("node_pubkey", tKeySign.localNodePubKey).Msg("finished signature preparation")
					}
				case common.MoneroSignShares:
					checkResp, err := tKeySign.walletAccessor.CheckTransaction(&wallet.RequestCheckTransaction{
						Destinations: txSend.Destinations,
						TxDataHex:    share.MultisigInfo,
					})
					if err != nil {
						tKeySign.logger.Err(err).Msg("fail to check transaction")
						globalErr = err
						notifyKeySignFinishedWithError()
						return
					}
					if !checkResp.CheckResult {
						err = errors.New("mutisig tx is not consistent with our keysign request")
						tKeySign.logger.Err(err).Msg("fail to check transaction")
						globalErr = err
						notifyKeySignFinishedWithError()
						return
					}
					accReq := wallet.RequestSignMultisig{
						TxDataHex: share.MultisigInfo,
					}
					signMultisigResp, err := tKeySign.walletAccessor.SignMultisig(&accReq)
					if err != nil {
						tKeySign.logger.Err(err).Msg("fail to sign multisig")
						globalErr = err
						notifyKeySignFinishedWithError()
						return
					}
					tKeySign.logger.Info().Msgf("sign multi-sig response: %+v", signMultisigResp.TxHashList)
					if len(signMultisigResp.TxHashList) == 0 {
						nextParty, err := tKeySign.getNextSigningParty(tKeySign.localNodePubKey, orderedNodes, partiesID)
						if err != nil {
							tKeySign.logger.Err(err).Msg("fail to find next signing party")
							globalErr = err
							notifyKeySignFinishedWithError()
							return
						}
						if nextParty == nil {
							globalErr = errors.New("can't find next signing party")
							notifyKeySignFinishedWithError()
							return
						}
						if err = tKeySign.packAndSend(signMultisigResp.TxDataHex, 1, localPartyID, nextParty, common.MoneroSignShares); err != nil {
							tKeySign.logger.Err(err).Msg("fail to send the initialization transfer info")
							globalErr = err
							notifyKeySignFinishedWithError()
							return
						}
						tKeySign.logger.Info().Msgf("we finished signing , and send it to next party: %s", nextParty.Id)
					} else {
						signedTx, err = tKeySign.submitAndGetConfirm(signMultisigResp.TxDataHex, txSend.Destinations[0].Address, memo)
						if err != nil {
							tKeySign.logger.Err(err).Msg("fail to submit the transaction")
							notifyKeySignFinishedWithError()
							// log the error , if submit transaction error , it is likely fine, we should treat it as success
							// because multisig wallet , there will have one who try first and submit successfully.
							if !strings.Contains(err.Error(), "transaction was rejected by daemon") {
								// treat it as an error
								globalErr = err
							}
							return
						}
						tKeySign.logger.Info().Msgf("transaction %s has been submitted successfully", signedTx.TransactionID)
					}
					if err := tKeySign.moneroCommonStruct.NotifyTaskDone(); err != nil {
						tKeySign.logger.Err(err).Msg("fail to broadcast keysign finished")
					}
					return
				}
			case <-tKeySign.moneroCommonStruct.GetTaskDone():
				return
			}
		}
	}()

	keySignWg.Wait()
	if globalErr != nil {
		tKeySign.logger.Error().Msgf("fail to create the monero signature with %s", tKeySign.GetTssCommonStruct().GetConf().KeyGenTimeout)
		lastMsg := blameMgr.GetLastMsg()
		if lastMsg == "" {
			tKeySign.logger.Error().Msg("fail to start the keysign, the last produced message of this node is none")
			return nil, errors.New("timeout before shared message is generated")
		}
		blameNodesBroadcast, err := blameMgr.GetBroadcastBlame(lastMsg)
		if err != nil {
			tKeySign.logger.Err(err).Msg("fail to get broadcast blame")
		}
		blameMgr.GetBlame().AddBlameNodes(blameNodesBroadcast...)
		return nil, globalErr
	}
	tKeySign.logger.Info().Msgf("%s successfully sign the message with TXID: %s ", tKeySign.p2pComm.GetHost().ID().String(), signedTx.TransactionID)
	return &signedTx, nil
}

// verifyTransaction use the wallet RPC to verify transaction
func (tKeySign *MoneroKeySign) verifyTransaction(receivedShare string, myDest []*wallet.Destination) (bool, error) {
	transactionCheck := wallet.RequestCheckTransaction{
		Destinations: myDest,
		TxDataHex:    receivedShare,
	}
	ret, err := tKeySign.walletAccessor.CheckTransaction(&transactionCheck)
	if err != nil {
		return false, fmt.Errorf("fail to check transaction,err: %w", err)
	}
	return ret.CheckResult, nil
}
