package keysign

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

// MockTxProofChecker a Mock of TxProofChecker
type MockTxProofChecker struct {
	mock.Mock
}

// CheckTxProof mock implementation of check tx proof
func (m *MockTxProofChecker) CheckTxProof(proof *wallet.RequestCheckTxProof) (*wallet.ResponseCheckTxProof, error) {
	args := m.Called(proof)
	return args.Get(0).(*wallet.ResponseCheckTxProof), args.Error(1)
}
