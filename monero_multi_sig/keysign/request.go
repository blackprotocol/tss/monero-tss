package keysign

// Request request to sign a message
type Request struct {
	BlockHeight   int64  `json:"block_height"`        // BlockHeight that this tx out is generated
	Version       string `json:"version"`             // TSS Version, currently is not used , for future-proof
	WalletAddress string `json:"wallet_address"`      // pub key of the pool that we would like to send this message from
	EncodedTx     string `json:"encoded_transaction"` // Transaction
	Memo          string `json:"memo"`                // Memo for the outbound tx
}

// NewRequest create a new keysign request
func NewRequest(walletAddress string, blockHeight int64, version, encodedTx, memo string) Request {
	return Request{
		WalletAddress: walletAddress,
		BlockHeight:   blockHeight,
		Version:       version,
		EncodedTx:     encodedTx,
		Memo:          memo,
	}
}
