package keysign

import (
	tb "gitlab.com/blackprotocol/tss/go-tss/blame"

	"gitlab.com/blackprotocol/tss/go-tss/common"
)

// Response key sign response
type Response struct {
	TxID      string        `json:"tx_id"`
	Signature string        `json:"signature"` // signature that can be used to proof the outbound
	Status    common.Status `json:"status"`
	Blame     tb.Blame      `json:"blame"`
}

// NewResponse create a new response
func NewResponse(txID, signature string, status common.Status, blame tb.Blame) Response {
	return Response{
		TxID:      txID,
		Signature: signature,
		Status:    status,
		Blame:     blame,
	}
}
