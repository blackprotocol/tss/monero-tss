package keysign

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/blackprotocol/monero-rpc/wallet"
)

type TxProofChecker interface {
	// CheckTxProof request wallet to check a given tx proof to see whether it is legitimate
	CheckTxProof(proof *wallet.RequestCheckTxProof) (*wallet.ResponseCheckTxProof, error)
}

// Notifier is design to receive keysign signature, success or failure
type Notifier struct {
	MessageID      string
	resp           chan *TxSendProof
	memo           string
	txSend         wallet.RequestTransfer
	txProofChecker TxProofChecker
	logger         zerolog.Logger
	threshold      int
}

// NewNotifier create a new instance of Notifier
func NewNotifier(messageID string, encodedTx string, memo string, txProofChecker TxProofChecker, threshold int) (*Notifier, error) {
	if len(messageID) == 0 {
		return nil, errors.New("messageID is empty")
	}
	if txProofChecker == nil {
		return nil, errors.New("TxProofChecker is nil")
	}
	if threshold == 0 {
		return nil, errors.New("threshold is 0")
	}
	if encodedTx == "" {
		return nil, errors.New("encodedTx is empty")
	}
	tx, err := base64.StdEncoding.DecodeString(encodedTx)
	if err != nil {
		return nil, fmt.Errorf("fail to decode tx,err: %w", err)
	}
	var txSend wallet.RequestTransfer
	err = json.Unmarshal(tx, &txSend)
	if err != nil {
		return nil, err
	}

	return &Notifier{
		MessageID:      messageID,
		memo:           memo,
		resp:           make(chan *TxSendProof, 1),
		txProofChecker: txProofChecker,
		txSend:         txSend,
		logger:         log.With().Str("module", "signature notifier Mn").Logger(),
		threshold:      threshold,
	}, nil
}

// verifySignature
func (n *Notifier) verifySignature(data *TxSendProof) (bool, error) {
	if n.txProofChecker == nil {
		return false, fmt.Errorf("TxProofChecker is nil, can't verify signature")
	}
	resp, err := n.txProofChecker.CheckTxProof(&wallet.RequestCheckTxProof{
		TxID:      data.TransactionID,
		Address:   n.txSend.Destinations[0].Address,
		Message:   n.memo,
		Signature: data.Signature,
	})
	if err != nil {
		return false, fmt.Errorf("fail to check tx proof,err: %w", err)
	}
	return resp.Good, nil
}

// ProcessSignature is to verify whether the signature is valid
// return value bool , true indicated we already gather all the signature from keysign party, and they are all match
// false means we are still waiting for more signature from keysign party
func (n *Notifier) ProcessSignature(data *TxSendProof) (bool, error) {
	if data == nil || data.TransactionID == "" || data.Signature == "" {
		n.logger.Debug().Msgf("received an key sign error")
		return false, nil
	}
	verify, err := n.verifySignature(data)
	if err != nil {
		return false, fmt.Errorf("fail to verify signature: %w", err)
	}
	if !verify {
		return false, errors.New("invalid signature")
	}
	n.logger.Info().Msg("received a valid tx")
	n.resp <- data
	return true, nil
}

// GetResponseChannel the final signature gathered from keysign party will be returned from the channel
func (n *Notifier) GetResponseChannel() <-chan *TxSendProof {
	return n.resp
}
