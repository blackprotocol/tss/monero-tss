module = gitlab.com/blackprotocol/tss/monero-tss

.PHONY: clear tools install test test-watch lint-pre lint lint-verbose protob build docker-gitlab-login docker-gitlab-push docker-gitlab-build

all: lint build

clear:
	clear

install: go.sum
	go install ./cmd/tss

go.sum: go.mod
	@echo "--> Ensure dependencies have not been modified"
	go mod verify

test:
	@go test ./...

test-watch: clear
	@gow -c test -tags testnet -mod=readonly ./...

lint-pre:
	@gofumpt -l cmd common monero_multi_sig monitor storage tss # for display
	@test -z "$(gofumpt -l cmd common monero_multi_sig monitor storage tss)" # cause error
	@go mod verify

lint: lint-pre
	@golangci-lint run

lint-verbose: lint-pre
	@golangci-lint run -v

build:
	go build ./...

# ------------------------------- GitLab ------------------------------- #
docker-gitlab-login:
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

docker-gitlab-push:
	docker push registry.github.com/akildemir/moneroTss

docker-gitlab-build:
	docker build -t registry.github.com/akildemir/moneroTss .
	docker tag registry.github.com/akildemir/moneroTss $$(git rev-parse --short HEAD)
